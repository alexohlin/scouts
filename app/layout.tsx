import { Inter } from "next/font/google";
import { ReactNode } from "react";
import "./globals.css";
import Footer from "@/components/Footer";
import { MainNavbar } from "@/components/MainNavbar";
import { Metadata } from "next";
import { ClerkProvider } from "@clerk/nextjs";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Scouts",
  description: "Scouts page",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: ReactNode;
}>) {
  return (
    <ClerkProvider>
      
      <html lang="en">
        <body>
          <MainNavbar />
          {children}
          <Footer />
        </body>
      </html>
    </ClerkProvider>
  );
}
