import ContactForm from "@/components/ContactForm";
import Image from "next/image";

export default function ContactPage() {
  return (
    <>
      <div
        className="py-28 md:px-5 bg-no-repeat bg-cover bg-center bg-opacity-60 bg-black bg-blend-darken mt-[-103px]"
        style={{
          backgroundImage: `url('/contact-us-bg.webp')`,
        }}
      >
        <div className="container mx-auto text-center md:-mt-[-103px]">
          <h1 className="uppercase text-center text-6xl">Contact Us</h1>
        </div>
      </div>
      <div className="container mx-auto py-28 text-black lg:px-5">
        <h2 className="uppercase text-center text-5xl mb-28">
          Lets Start A Conversation
        </h2>
        <div className="grid grid-cols-2 md:gap-5">
          <div className="col-span-2 md:col-span-1 text-2xl text-center mb-28 md:mb-0">
            <div>
              <a href="#" className="hover:text-green-600 block mb-5">
                sim@scout.org.mk
              </a>
              <a href="#" className="hover:text-green-600 block mb-5">
                02 311 2254
              </a>
              <div>
                <a
                  href="/#"
                  className="
                hover:text-green-600 inline-flex mr-2"
                >
                  <Image
                    src="/tiktok.png"
                    alt="tiktok icon"
                    width={40}
                    height={40}
                    style={{ height: "40px" }}
                    className="object-contain"
                  />
                </a>
                <a
                  href="/#"
                  className="
                hover:text-green-600 inline-flex mr-2"
                >
                  <Image
                    src="/facebook.png"
                    alt="facebook icon"
                    width={40}
                    height={40}
                    style={{ height: "40px" }}
                    className="object-contain"
                  />
                </a>
                <a
                  href="#"
                  className="
            hover:text-green-600 inline-flex"
                >
                  <Image
                    src="/instagram.png"
                    alt="instagram icon"
                    width={40}
                    height={40}
                    style={{ height: "40px" }}
                    className="object-contain"
                  />
                </a>
              </div>
            </div>
          </div>
          <div className="col-span-2 md:col-span-1">
            <ContactForm />
          </div>
        </div>
      </div>
    </>
  );
}
