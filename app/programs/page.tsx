import Image from "next/image";
import Link from "next/link";

export default function Programs() {
  return (
    <>
      <div
        className="py-28 md:px-5 bg-no-repeat bg-cover bg-center bg-opacity-30 bg-black bg-blend-darken mt-[-103px]"
        style={{
          backgroundImage: `url('/bg2.jpg')`,
        }}
      >
        <div className="container mx-auto text-center md:-mt-[-103px]">
          <h1 className="uppercase text-5xl mb-14">Scout Program</h1>
          <p>
            A program for children and youth based on their needs and
            aspirations
          </p>
        </div>
      </div>
      <div
        className="py-28 md:px-5 bg-no-repeat bg-cover md:bg-contain bg-right-bottom bg-white bg-opacity-80 bg-blend-screen"
        style={{
          backgroundImage: `url('/bg3.png')`,
        }}
      >
        <div className="container mx-auto text-center text-black w-3/4">
          <p>
            The Children and Youth Program in Scouting is a collection of
            educational opportunities that youth benefit from (What), designed
            to contribute to the achievement of Scoutings purpose (Why), and
            experienced through the Scouting method (How).
          </p>
        </div>
      </div>
      <div
        className="py-28 md:px-5 bg-no-repeat bg-left-top bg-contain bg-green-950"
        style={{
          backgroundImage: `url('/forms.svg')`,
        }}
      >
        <div className="container mx-auto grid grid-cols-1 md:grid-cols-4 gap-5">
          <Image
            className="rounded-lg w-full h-full"
            src={"/book1.png"}
            alt={"book1"}
            height={500}
            width={500}
          />
          <Image
            className="rounded-lg w-full h-full"
            src={"/book2.png"}
            alt={"book2"}
            height={500}
            width={500}
          />
          <Image
            className="rounded-lg w-full h-full"
            src={"/book1.png"}
            alt={"book1"}
            height={500}
            width={500}
          />
          <Image
            className="rounded-lg w-full h-full"
            src={"/book2.png"}
            alt={"book2"}
            height={500}
            width={500}
          />
        </div>
      </div>
      <div className="py-28 md:px-5 bg-white">
        <div className="container mx-auto">
          <div className="bg-green-950 flex flex-wrap rounded-3xl mb-20 relative">
            <Image
              className="absolute top-0 left-0 bottom-0"
              src={"/forms.svg"}
              alt={"forms"}
              height={300}
              width={300}
            />
            <div className="p-10 w-full md:w-1/2 z-10">
              <h2 className="text-4xl mb-10">JUNIORS (7 - 10 years)</h2>
              <p className="mb-10">
                They get to know nature, learn to love it and take care of it.
                They learn how to navigate in nature and become aware of its
                values and dangers. They know their rights and obligations and
                develop a sense of justice and equality. They get to know,
                respect, and cooperate with the world they live in. They freely
                explore their thoughts and feelings. They take care of their
                health and spirit. They navigate through time and space,
                creatively explore, use basic technical devices, and
                communication tools. They value their own culture and that of
                others, accept the code of good conduct and ethics. They become
                familiar with the work and principles of the organization,
                participating in building their patrol, learning to respect,
                value, and love scouting, knowing their rights and duties within
                the organization.
              </p>
              <Link
                href={"/programs/juniors"}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block"
              >
                The program for JUNIORS
              </Link>
            </div>
            <div className="md:w-1/2 relative rounded-b-3xl">
              <Image
                className="hidden md:block md:rounded-e-3xl md:absolute object-cover"
                src={"/scouts.jpg"}
                alt={"scouts"}
                fill
              />
              <Image
                className="block md:hidden w-full rounded-b-3xl"
                src={"/scouts.jpg"}
                alt={"scouts"}
                height={500}
                width={500}
              />
            </div>
          </div>
          <div className="bg-green-950 flex flex-wrap rounded-3xl mb-20">
            <div className="relative p-10 w-full md:w-1/2 order-first md:order-last">
              <Image
                className="absolute rotate-180 right-0 bottom-0"
                src={"/forms.svg"}
                alt={"forms"}
                height={300}
                width={300}
              />
              <h2 className="text-4xl mb-10 z-20 relative">
                SCOUTS (11 - 14 years)
              </h2>
              <p className="mb-10 z-20 relative">
                They explore, observe, evaluate, notice, and navigate their
                environment. In doing so, they learn to live in harmony with
                nature without disturbing it. They embrace civic values and
                respect their own culture and the cultures of others, thus
                contributing to the development of their community. They
                understand the general rules of behavior and communication, keep
                up with trends, understand their cultural and spiritual
                heritage, as well as that of others, thereby developing into
                conscious and responsible individuals who plan resources and
                take care of health and hygiene. They take pride as members of
                the organization and appreciate its values and principles,
                understanding the structure of the organization, their rights
                and responsibilities, its traditions, and its development in the
                country and the world.
              </p>
              <Link
                href={""}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block z-20 relative"
              >
                The program for SCOUTS
              </Link>
            </div>
            <div className="md:w-1/2 relative rounded-b-3xl">
              <Image
                className="hidden md:block md:rounded-s-3xl md:absolute object-cover"
                src={"/scouts2.jpg"}
                alt={"scouts2"}
                fill
              />
              <Image
                className="block md:hidden w-full rounded-b-3xl"
                src={"/scouts2.jpg"}
                alt={"scouts2"}
                height={500}
                width={500}
              />
            </div>
          </div>
          <div className="bg-green-950 flex flex-wrap rounded-3xl mb-20 relative">
            <Image
              className="absolute top-0 left-0 bottom-0"
              src={"/forms.svg"}
              alt={"forms"}
              height={300}
              width={300}
            />
            <div className="relative p-10 w-full md:w-1/2">
              <h2 className="text-4xl mb-10">RESEARCHERS (15 - 17 years)</h2>
              <p className="mb-10">
                They camp in nature and use natural resources rationally. They
                are capable of surviving independently in nature and navigating
                the wilderness. They respect the natural environment, take
                responsibility for it, and pass on this approach to others. They
                respect and understand others and develop universal human
                values. They advocate for peace and human rights, thereby
                creating a world free from prejudice, stereotypes, and all forms
                of discrimination. They contribute to the preservation of
                tradition and culture. They are responsible and innovative
                individuals with a developed identity, capable of actively
                participating in social life and living in harmony with the
                spiritual and social reality of the community. They understand
                the organizational structure and participate in preserving its
                values, culture, and tradition; they participate in implementing
                the program, making systematic decisions, and advancing the
                work; as well as exchanging experiences with other organizations
                within the scouting family and beyond, while safeguarding and
                representing the values of scouting.
              </p>
              <Link
                href={""}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block"
              >
                The program for RESEARCHERS
              </Link>
            </div>
            <div className="md:w-1/2 relative rounded-b-3xl">
              <Image
                className="hidden md:block md:rounded-e-3xl md:absolute object-cover"
                src={"/researchers.jpg"}
                alt={"researchers"}
                fill
              />
              <Image
                className="block md:hidden w-full rounded-b-3xl"
                src={"/researchers.jpg"}
                alt={"researchers"}
                height={500}
                width={500}
              />
            </div>
          </div>
          <div className="bg-green-950 flex flex-wrap rounded-3xl mb-20">
            <div className="relative p-10 w-full md:w-1/2 order-first md:order-last">
              <Image
                className="absolute rotate-180 right-0 bottom-0"
                src={"/forms.svg"}
                alt={"forms"}
                height={300}
                width={300}
              />
              <h2 className="text-4xl mb-10 relative">
                ROVERS (18 - 29 years)
              </h2>
              <p className="mb-10 relative">
                Rover Scouts organize themselves into clubs, which are the
                primary forms of organization and working groups through which
                they implement their program and social activities. Within the
                clubs, sections and groups are formed for easier implementation
                of activities, which can be of a permanent or temporary nature,
                depending on the preferences of the membership and the types of
                activities they want to engage in. The club operates within the
                framework of the scout troop, in full synchronization and
                coordination with the work of other members, and occasionally
                may join forces with other clubs at all levels of organization.
                The club has its own name and emblem or flag, in accordance with
                the applicable regulations. A Rover Scout club can be formed by
                multiple Rover Scouts from different scout troops, students, and
                similar groups.
              </p>
              <Link
                href={""}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block relative -z-0"
              >
                The program for ROVERS
              </Link>
            </div>
            <div className="md:w-1/2 relative rounded-b-3xl">
              <Image
                className="hidden md:block md:rounded-s-3xl md:absolute object-cover"
                src={"/rovers.jpg"}
                alt={"rovers"}
                fill
              />
              <Image
                className="block md:hidden w-full rounded-b-3xl"
                src={"/rovers.jpg"}
                alt={"rovers"}
                height={500}
                width={500}
              />
            </div>
          </div>
          <div className="bg-green-950 flex flex-wrap rounded-3xl relative">
            <Image
              className="absolute top-0 left-0 bottom-0"
              src={"/forms.svg"}
              alt={"forms"}
              height={300}
              width={300}
            />
            <div className="relative p-10 w-full md:w-1/2">
              <h2 className="text-4xl mb-10">SENIORS (30+ years)</h2>
              <p className="mb-10">
                Do you have a passion for outdoor activities? Do you support a
                healthy and active lifestyle as the foundation for a better
                quality of life? Do you believe you have something to offer in
                informal education for young people? Do you want to contribute
                to the development of young people in your community? Do you
                want to create a better world for the youth?{" "}
                {
                  <>
                    <br />
                    <br />
                  </>
                }
                Adults can be crucial in guiding young people to choose the
                right path in their lives. Adults can become part of the
                scouting organization, which provides space and opportunities
                for every adult in line with their free time and desire to
                contribute to scouting. Through the scouting organization,
                adults can build their self-confidence and develop
                organizational and leadership skills. The involvement of adults
                in scouting is always welcome both locally and nationally.
              </p>
              <Link
                href={""}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block"
              >
                The program for SENIORS
              </Link>
            </div>
            <div className="w-full md:w-1/2 relative rounded-b-3xl">
              <Image
                className="hidden md:block md:rounded-e-3xl md:absolute object-cover"
                src={"/seniors.jpg"}
                alt={"seniors"}
                fill
              />
              <Image
                className="block md:hidden w-full rounded-b-3xl"
                src={"/seniors.jpg"}
                alt={"seniors"}
                height={600}
                width={600}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="pb-28 md:px-5 bg-white">
        <div className="container mx-auto text-black">
          <div className="flex flex-wrap rounded-3xl items-center">
            <div className="relative p-10 w-full md:w-1/2">
              <h2 className="text-4xl mb-10"> SCOUTING SKILLS</h2>
              <p className="mb-10">
                The program for learning and mastering skills in the scouting
                organization represents a special program consisting of more
                than 50 practical life skills, aimed at guiding scouts towards
                their proficiency in specific skills and their improvement.
                Through this program, scouts are directed towards the
                improvement of the proposed skills, thereby achieving personal
                development in multiple fields. The skills program is
                complementary to the basic scouting program of the organization.
              </p>
              <Link
                href={""}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block"
              >
                More about the scouting skills
              </Link>
            </div>
            <div className="md:w-1/2 w-full">
              <div className="grid grid-cols-3 text-center">
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (1).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (2).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (3).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (4).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (5).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto mb-10"
                  src={"/vestarstva (6).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto"
                  src={"/vestarstva (7).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto"
                  src={"/vestarstva (8).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
                <Image
                  className="w-fill mx-auto"
                  src={"/vestarstva (9).png"}
                  alt={"skills"}
                  height={100}
                  width={100}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
