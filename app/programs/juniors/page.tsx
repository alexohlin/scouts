import Image from "next/image";
import Link from "next/link";

export default function Juniors() {
  return (
    <>
      <div
        className="py-28 px-5 lg:px-10 bg-no-repeat bg-cover bg-center bg-opacity-30 bg-black bg-blend-darken"
        style={{
          backgroundImage: `url('/juniors-bg.jpg')`,
          marginTop: "-103px",
        }}
      >
        <h1
          style={{ marginTop: "50px" }}
          className="container mx-auto text-center uppercase text-5xl"
        >
          Juniors
        </h1>
      </div>
      <div className="py-28 md:px-5 container mx-auto text-center text-black">
        <p className="mb-28">
          They get to know nature, learn to love it, and take care of it. They
          learn how to navigate in nature and become aware of its values and
          dangers. They know their rights and responsibilities and develop a
          sense of justice and equality. They get to know, respect, and
          cooperate with the world they live in. They freely explore their
          thoughts and feelings. They take care of their health and spirit. They
          navigate through time and space, creatively explore, use basic
          technical devices, and means of communication. They appreciate their
          own and the culture of others, accept the code of good conduct and
          ethics. They familiarize themselves with the work and principles of
          the organization, participating in the construction of their patrol,
          learning to respect, value, and love scouting, knowing their rights
          and duties in the organization.
        </p>
        <div className="grid md:grid-cols-2 grid-cols-1 gap-5 text-white mb-28">
          <div className="bg-green-950 flex flex-wrap rounded-3xl relative">
            <Image
              className="absolute -rotate-90 left-1 -bottom-1"
              src={"/forms.svg"}
              alt={"forms"}
              height={200}
              width={200}
            />
            <Image
              className="w-full h-96 object-cover rounded-3xl relative z-10"
              src={"/scouts.jpg"}
              alt={"scouts"}
              height={400}
              width={400}
            />
            <div className="p-10 w-full z-10">
              <p>
                The program for scouts consists of general and specific
                knowledge, which are based on the original ideas of the founder
                of the scouting movement. Through the implementation of this
                program, scouts achieve progress in their intellectual,
                physical, emotional, social, spiritual, and character
                development.
              </p>
            </div>
          </div>
          <div className="bg-green-950 flex flex-wrap rounded-3xl relative">
            <Image
              className="absolute rotate-180 right-1 bottom-1"
              src={"/forms.svg"}
              alt={"forms"}
              height={200}
              width={200}
            />
            <Image
              className="w-full h-96 object-cover rounded-3xl relative z-10"
              src={"/scouts.jpg"}
              alt={"scouts"}
              height={400}
              width={400}
            />
            <div className="p-10 w-full z-10">
              <p className="mb-10">
                Embark on a different adventure! Join the largest youth movement
                in Macedonia.
              </p>
              <Link
                href={"/programs/juniors"}
                className="text-center uppercase border-b-orange-400 border-b-2 pb-5 block"
              >
                Become a member
              </Link>
            </div>
          </div>
          <div className="lg:p-10">
            <svg
              width="90%"
              fill="none"
              viewBox="-50 -50 659 745"
              style={{ overflow: "visible" }}
              className="mx-auto"
            >
              <path
                fill="#24463B"
                stroke="#EF767A"
                strokeWidth="3"
                d="M2.28 162.115 279.638 1.733 557 162.115v320.77L279.639 643.267 2.279 482.885v-320.77Z"
              />
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="-50"
                y="125"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/physical.svg"
                x="-40"
                y="135"
                width="100"
                height="100"
              />
              <text fontSize={30} x="-45" y="105" fill="black">
                Phisical
              </text>
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="490"
                y="125"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/relational.svg"
                x="500"
                y="135"
                width="100"
                height="100"
              />
              <text fontSize={30} x="485" y="105" fill="black">
                Relational
              </text>
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="220"
                y="-20"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/intelectual.svg"
                x="225"
                y="-19"
                width="110"
                height="110"
              />
              <text fontSize={30} x="210" y="-40" fill="black">
                Intelectual
              </text>
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="220"
                y="550"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/spiritual.svg"
                x="230"
                y="555"
                width="100"
                height="100"
              />
              <text fontSize={30} x="220" y="710" fill="black">
                Spiritual
              </text>
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="-50"
                y="400"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/social.svg"
                x="-40"
                y="410"
                width="100"
                height="100"
              />
              <text fontSize={30} x="-30" y="560" fill="black">
                Social
              </text>
              <rect
                stroke="#EF767A"
                strokeWidth="5"
                x="490"
                y="400"
                width="120"
                height="120"
                rx="10"
                ry="10"
                fill="#24463B"
              />
              <image
                href="/icons/characteristic.svg"
                x="500"
                y="410"
                width="100"
                height="100"
              />
              <text fontSize={30} x="460" y="560" fill="black">
                Characteristic
              </text>
              <image
                href="/icons/hand.svg"
                x="200px"
                y="250"
                width="150"
                height="150"
              />
            </svg>
          </div>
          <div className="bg-green-950 rounded-3xl relative flex items-center">
            <Image
              className="absolute bottom-0 right-0 rotate-180"
              src={"/forms.svg"}
              alt={"forms"}
              height={200}
              width={200}
            />
            <div className="p-10 z-10 relative">
              <h2 className="text-3xl mb-10">
                AREAS OF PERSONAL DEVELOPMENT FOR SCOUTS IN THE SCOUTING
                PROGRAM.
              </h2>
              <p>
                Through carefully designed activities and learning methods,
                progress in these 6 developmental fields is enabled for scouts.
                New skills and friendships are on the horizon!
              </p>
            </div>
          </div>
        </div>
        <h2 className="text-4xl uppercase mb-28">testimonials</h2>
        <div className="mb-36 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-x-5 gap-y-20 lg:gap-y-0 text-white">
          <div className="bg-green-950 rounded-xl relative">
            <Image
              className="absolute -bottom-[52px] left-[50%] -translate-x-2/4"
              src={"/icons/triangle.svg"}
              alt={"triangle figures"}
              height={100}
              width={100}
            />
            <Image
              className="rounded-xl h-96 object-cover w-full"
              src={"/testimonials/testimonia (1).jpg"}
              alt={"testimonial"}
              height={500}
              width={500}
            />
            <div className=" p-5 md:py-20 md:px-10">
              <p className="mb-8">
                I am Ana, a scout for two years now. Im here for the friendship,
                the activities, and what I like the most is that the scouts are
                like a family to me. I visit all the camps, and my favorite is
                the summer camp in ICO (2022). My wish for the future is to be a
                patrol leader.
              </p>
              <p>Ana, 10 years old.</p>
            </div>
          </div>
          <div className="bg-green-950 rounded-xl relative">
            <Image
              className="absolute -bottom-[52px] left-[50%] -translate-x-2/4"
              src={"/icons/triangle.svg"}
              alt={"triangle figures"}
              height={100}
              width={100}
            />
            <Image
              className="rounded-xl h-96 object-cover w-full"
              src={"/testimonials/testimonia (2).jpg"}
              alt={"testimonial"}
              height={500}
              width={500}
            />
            <div className=" p-5 md:py-20 md:px-10">
              <p className="mb-8">
                I am Ana, a scout for two years now. Im here for the friendship,
                the activities, and what I like the most is that the scouts are
                like a family to me. I visit all the camps, and my favorite is
                the summer camp in ICO (2022). My wish for the future is to be a
                patrol leader.
              </p>
              <p>Ana, 10 years old.</p>
            </div>
          </div>
          <div className="bg-green-950 rounded-xl relative">
            <Image
              className="absolute -bottom-[52px] left-[50%] -translate-x-2/4"
              src={"/icons/triangle.svg"}
              alt={"triangle figures"}
              height={100}
              width={100}
            />
            <Image
              className="rounded-xl h-96 object-cover w-full"
              src={"/testimonials/testimonia (3).jpg"}
              alt={"testimonial"}
              height={500}
              width={500}
            />
            <div className=" p-5 md:py-20 md:px-10">
              <p className="mb-8">
                I am Ana, a scout for two years now. Im here for the friendship,
                the activities, and what I like the most is that the scouts are
                like a family to me. I visit all the camps, and my favorite is
                the summer camp in ICO (2022). My wish for the future is to be a
                patrol leader.
              </p>
              <p>Ana, 10 years old.</p>
            </div>
          </div>
          <div className="bg-green-950 rounded-xl relative">
            <Image
              className="absolute -bottom-[52px] left-[50%] -translate-x-2/4"
              src={"/icons/triangle.svg"}
              alt={"triangle figures"}
              height={100}
              width={100}
            />
            <Image
              className="rounded-xl h-96 object-cover w-full"
              src={"/testimonials/testimonia (4).jpg"}
              alt={"testimonial"}
              height={500}
              width={500}
            />
            <div className=" p-5 md:py-20 md:px-10">
              <p className="mb-8">
                I am Ana, a scout for two years now. Im here for the friendship,
                the activities, and what I like the most is that the scouts are
                like a family to me. I visit all the camps, and my favorite is
                the summer camp in ICO (2022). My wish for the future is to be a
                patrol leader.
              </p>
              <p>Ana, 10 years old.</p>
            </div>
          </div>
        </div>
        <Link
          className="text-2xl lg:text-5xl uppercase mb-28 border-b-orange-400 border-b-2 pb-5 block"
          href={""}
        >
          Become a member
        </Link>
        <h2 className="text-4xl uppercase mb-28">Events</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
          <div className="relative">
            <Image
              className="w-full md:h-[920px] object-cover rounded-3xl"
              src={"/scouts.jpg"}
              alt={"scouts"}
              height={400}
              width={400}
            />
            <Link
              className="text-xl left-1/2 -translate-x-1/2 absolute text-white bottom-16 uppercase border-b-orange-400 border-b-2"
              href={""}
            >
              Next Event
            </Link>
          </div>
          <div className="grid gap-y-5">
            <div className="relative">
              <Image
                className="w-full md:h-[450px] object-cover rounded-3xl"
                src={"/scouts.jpg"}
                alt={"scouts"}
                height={400}
                width={400}
              />
              <Link
                className="text-xl left-1/2 -translate-x-1/2 absolute text-white bottom-16 uppercase border-b-orange-400 border-b-2"
                href={""}
              >
                Next Event
              </Link>
            </div>
            <div className="relative">
              <Image
                className="w-full md:h-[450px] object-cover rounded-3xl"
                src={"/scouts.jpg"}
                alt={"scouts"}
                height={400}
                width={400}
              />
              <Link
                className="text-xl left-1/2 -translate-x-1/2 absolute text-white bottom-16 uppercase border-b-orange-400 border-b-2"
                href={""}
              >
                Next Event
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
