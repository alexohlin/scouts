"use client";

import Link from "next/link";
import Image from "next/image";
import DonateButton from "@/components/DonateButton";
import { useUser } from "@clerk/nextjs";

export default function Home() {
  const { isSignedIn } = useUser();

  return (
    <>
      <div
        className="bg-no-repeat bg-cover bg-opacity-50 bg-black bg-blend-darken mt-[-103px]"
        style={{
          backgroundImage: `url('/homebackground.png')`,
        }}
      >
        <div className="min-h-screen py-28 md:px-5 container flex flex-col mx-auto flex-wrap justify-between">
          <Image
            src="logo1.svg"
            alt="logo"
            width={100}
            height={100}
            className="mb-20 mx-auto"
          />
          <h1 className="uppercase text-center text-6xl mb-20">Scouts</h1>
          <div className="text-end">
            {!isSignedIn && (
              <Link
                href={""}
                className="bg-white px-8 py-5 mr-5 lg:mr-10 text-black rounded-md inline-block"
              >
                Register
              </Link>
            )}
            <DonateButton styleClasses="bg-transparent px-8 py-5 mr-5 lg:mr-10 border border-white rounded-md inline-block" />
          </div>
        </div>
      </div>
      <div className="bg-gradient-to-b from-black to-green-950">
        <div className="container mx-auto pt-28 md:px-5">
          <div className="grid grid-cols-1 md:grid-cols-3 gap-5 mb-28">
            <div className="bg-green-950 rounded-xl">
              <Image
                className="rounded-xl h-96 object-cover"
                src={"/what.png"}
                alt={"what"}
                height={1000}
                width={1000}
              />
              <div className="p-10 mb:p-20 text-center">
                <h2 className="mb-10 uppercase text-5xl">WHAT?</h2>
                <p className="border-y-orange-400 border-y-2 py-5">
                  Scouting is a voluntary, non-political, educational movement
                  for children and youth, open to all regardless of sex, origin,
                  race or creed, in accordance with the purpose, principles and
                  method envisioned by the founder (Baden Powell).
                </p>
              </div>
            </div>
            <div className="bg-green-950 rounded-xl">
              <Image
                className="rounded-xl h-96 object-cover"
                src={"/how.png"}
                alt={"how"}
                height={1000}
                width={1000}
              />
              <div className="p-10 mb:p-20 text-center">
                <h2 className="mb-10 uppercase text-5xl">HOW??</h2>
                <p className="border-y-orange-400 border-y-2 py-5">
                  Through a system of progressive self-education, with the
                  interaction of the elements of the scouting method. The
                  scouting method has eight constituent elements, equally
                  important in the education of children and young people, and
                  they represent a key approach and specificity for scouting
                  compared to other educational movements / organizations.
                </p>
              </div>
            </div>
            <div className="bg-green-950 rounded-xl">
              <Image
                className="rounded-xl h-96 object-cover"
                src={"/why.png"}
                alt={"why"}
                height={1000}
                width={1000}
              />
              <div className="p-10 mb:p-20 text-center">
                <h2 className="mb-10 uppercase text-5xl">WHY?</h2>
                <p className="border-y-orange-400 border-y-2 py-5">
                  The goal of the scouting movement is to contribute to the
                  development of young people in achieving their full physical,
                  intellectual, emotional, social and spiritual potential as
                  individuals, as responsible citizens and as members of their
                  local, national and international communities.
                </p>
              </div>
            </div>
          </div>
          <div className="flex items-center flex-col relative mb-28 md:px-5">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 444 305"
              fill="none"
              className=" max-w-sm md:max-w-lg"
              style={{ top: "180px", height: "auto" }}
            >
              <path
                d="M0 128.5V255.5L55.5 256C55.5 275 69.5 305 104 305C137.5 305 151.5 270.667 150.5 256H293.5C293.167 240.667 306 209.5 339.5 209.5C377.102 209.5 387.833 240.333 388.5 256H444V129L222 0.5L0 128.5Z"
                fill="#213C33"
                stroke="#EF767A"
                strokeWidth="3"
              />
              <text
                x="50%"
                y="50%"
                dominantBaseline="middle"
                textAnchor="middle"
                fill="white"
                fontSize="18"
              >
                Empowerment of the autonomous individual
              </text>
            </svg>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 444 305"
              fill="none"
              className="max-w-sm md:max-w-lg -mt-24 md:-mt-32"
              style={{ height: "auto" }}
            >
              <path
                d="M444 177V50L388.5 49.5C388.5 30.5 374.5 0.5 340 0.5C306.5 0.5 292.5 34.8333 293.5 49.5H150.5C150.833 64.8333 138 96 104.5 96C66.8979 96 56.1667 65.1667 55.5 49.5H0V176.5L222 305L444 177Z"
                fill="#213C33"
                stroke="#EF767A"
                strokeWidth="3"
              />{" "}
              <text
                x="50%"
                y="50%"
                dominantBaseline="middle"
                textAnchor="middle"
                fill="white"
                fontSize="18"
              >
                Developing an active global citizen
              </text>
            </svg>
          </div>
          <div className="mb-28 text-center text-white">
            <h2 className="uppercase text-5xl mb-14">Be a part of our team!</h2>
            <p>
              With more than 57 million active members in 173 countries and
              territories, Scouting is one of the largest youth movements in the
              world! From archery and hiking to public speaking and leadership
              training, Scouts learn by doing and develop skills that will help
              them thrive and become agents of positive change in our world.
              Contact your local Scout troop to find out how to start your
              Scouting journey.
            </p>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-5 gap-5">
            <div>
              <Image
                className="h-96 object-cover rounded-t-3xl"
                src={"/juniors.png"}
                alt={"juniors"}
                height={1000}
                width={1000}
              />
              <div className="text-center bg-gradient-to-b from-65% to-0 py-5">
                <p className="mb-5 uppercase text-2xl">Juniors</p>
                <p> Children from 7 to 10 years</p>
              </div>
            </div>
            <div>
              <Image
                className="h-96 object-cover rounded-t-3xl"
                src={"/scouts.png"}
                alt={"kids"}
                height={1000}
                width={1000}
              />
              <div className="text-center bg-gradient-to-b from-65% to-0 py-5">
                <p className="mb-5 uppercase text-2xl">Scouts</p>
                <p> Children from 11 to 14 years</p>
              </div>
            </div>

            <div>
              <Image
                className="h-96 object-cover rounded-t-3xl"
                src={"/researchers.png"}
                alt={"researchers"}
                height={1000}
                width={1000}
              />
              <div className="text-center bg-gradient-to-b from-65% to-0 py-5">
                <p className="mb-5 uppercase text-2xl">Researchers</p>
                <p>Teens from 15 to 17 years</p>
              </div>
            </div>

            <div>
              <Image
                className="h-96 object-cover rounded-t-3xl"
                src={"/rovers.png"}
                alt={"rovers"}
                height={1000}
                width={1000}
              />
              <div className="text-center bg-gradient-to-b from-65% to-0 py-5">
                <p className="mb-5 uppercase text-2xl">Rovers</p>
                <p>Young adults from 18 to 29 years</p>
              </div>
            </div>

            <div>
              <Image
                className="h-96 object-cover rounded-t-3xl"
                src={"/seniors.png"}
                alt={"seniors"}
                height={1000}
                width={1000}
              />
              <div className="text-center bg-gradient-to-b from-65% to-0 py-5">
                <p className="mb-5 uppercase text-2xl">Seniors</p>
                <p>Adults 30+ years</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-gradient-to-b pb-28 from-green-950 to-black">
        <div className="container mx-auto relative flex flex-col md:px-5">
          <q className="text-center container text-white mx-auto lg:px-40 pt-28 pb-28 text-3xl italic">
            I give my honest word that I will know and love my homeland, that I
            will accept spiritual reality and seek its full meaning. That I will
            always help people, that I will live and work by the Scout Laws.
          </q>
          <Image
            src={"/hand.svg"}
            alt={"background"}
            height={50}
            width={50}
            className="mx-auto"
          />
        </div>
      </div>
      <div className="text-center bg-black bg-gradient-to-b from-black via-green-950 to-white">
        <div className="container mx-auto mb-28 md:px-5">
          <h2 className="uppercase text-5xl mb-14">
            A new adventure is waiting for you!
          </h2>
          <p>
            Regardless of gender, age, background or determination, anyone
            interested can join and contribute to the largest youth educational
            movement in Macedonia, in one or more ways:
          </p>
        </div>
        <div className="mb-28 text-center container mx-auto md:px-5">
          <div className="grid grid-cols-1 md:grid-cols-4 gap-5">
            <div>
              <Image
                className="rounded-xl h-96 object-cover border-4 border-orange-400"
                src={"/join.png"}
                alt={"Join us"}
                height={1000}
                width={1000}
              />
              <div className="pt-10 text-center">
                <p className="uppercase text-2xl border-t-orange-400 border-t-2 pt-5">
                  Join Us
                </p>
              </div>
            </div>
            <div>
              <Image
                className="rounded-xl h-96 object-cover border-4 border-orange-400"
                src={"/volunteer.png"}
                alt={"volunteer"}
                height={1000}
                width={1000}
              />

              <div className="pt-10 text-center">
                <p className="uppercase text-2xl border-t-orange-400 border-t-2 pt-5">
                  Volunteer
                </p>
              </div>
            </div>
            <div>
              <Image
                className="rounded-xl h-96 object-cover border-4 border-orange-400"
                src={"/donate.png"}
                alt={"donate"}
                height={1000}
                width={1000}
              />
              <div className="pt-10 text-center">
                <p className="uppercase text-2xl border-t-orange-400 border-t-2 pt-5">
                  Donate
                </p>
              </div>
            </div>
            <div>
              <Image
                className="rounded-xl h-96 object-cover border-4 border-orange-400"
                src={"/open-squad.png"}
                alt={"open-squad"}
                height={1000}
                width={1000}
              />
              <div className="pt-10 text-center">
                <p className="uppercase text-2xl border-t-orange-400 border-t-2 pt-5">
                  Open a Squad
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="mb-28 text-center container mx-auto md:px-5">
          <h2 className="uppercase text-5xl mb-14">Events</h2>
          <p>
            SIM, as well as more than 17 scout detachments throughout Macedonia,
            annually organize one-day and multi-day activities, hikes,
            trainings, trainings, workshops and camping. Join us!
          </p>
        </div>
        <div className="text-center container mx-auto mb-28 md:px-5">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-5 lg:px-52">
            <Image
              className="rounded-lg w-full h-96 md:h-full object-cover"
              src={"/event1.png"}
              alt={"event1"}
              height={500}
              width={500}
            />
            <div className="gap-5 grid">
              <Image
                className="rounded-lg w-full h-96 md:h-full object-cover"
                src={"/event2.png"}
                alt={"event"}
                height={500}
                width={500}
              />
              <Image
                className="rounded-lg w-full h-96 md:h-full object-cover"
                src={"/event3.png"}
                alt={"event"}
                height={500}
                width={500}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
