"use server";

import EmailTemplate from "@/components/Email-template";
import { Resend } from "resend";

export async function ContactFormAction(formData: FormData) {
  const email = formData.get("email") as string;
  const firstname = formData.get("firstname") as string;
  const lastname = formData.get("lastname") as string;
  const message = formData.get("message") as string;
  const resend = new Resend(process.env.RESEND_API_KEY);

  try {
    await resend.emails.send({
      from: "onboarding@resend.dev",
      to: "aleksandarcvetanovskiristov@gmail.com",
      subject: `Message from ${firstname}`,
      react: (
        <EmailTemplate
          firstname={firstname}
          lastname={lastname}
          message={message}
          email={email}
        />
      ),
    });
  } catch (error) {
    return {
      error,
    };
  }
}
