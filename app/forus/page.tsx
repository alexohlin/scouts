import Image from "next/image";

export default function ForUs() {
  return (
    <>
      <div
        className="py-28 md:px-5 bg-no-repeat bg-cover bg-center bg-opacity-50 bg-black bg-blend-darken  mt-[-103px]"
        style={{
          backgroundImage: `url('/forus.png')`,
        }}
      >
        <div className="container mx-auto text-center md:mt-[103px]">
          <h1 className="uppercase text-5xl">For us</h1>
        </div>
      </div>
      <div className="py-28 md:px-5 lg:px-10 bg-no-repeat bg-bottom relative">
        <Image
          className="absolute left-0 bottom-0 w-full"
          src={"/forms1.svg"}
          alt={"forms"}
          height={300}
          width={300}
        />
        <div className="container mx-auto text-center flex flex-wrap text-3xl md:text-4xl font-semibold uppercase md:mb-16 text-zinc-800">
          <div className="w-full mb-10 md:mb-0 md:w-1/5 md:pr-5">
            <p className="mb-2 md:mb-5">70</p>
            <p>years</p>
          </div>
          <div className="w-full mb-10 md:mb-0 md:w-1/5 md:pr-5">
            <p className="mb-2 md:mb-5">17</p>
            <p>cities</p>
          </div>
          <div className="w-full mb-10 md:mb-0 md:w-1/5 md:pr-5">
            <p className="mb-2 md:mb-5">2500+</p>
            <p>members</p>
          </div>
          <div className="w-full mb-10 md:mb-0 md:w-1/5 md:pr-5">
            <p className="mb-2 md:mb-5">280+</p>
            <p>activities</p>
          </div>
          <div className="w-full mb-10 md:mb-0 md:w-1/5">
            <p className="mb-2 md:mb-5">52+</p>
            <p>skills</p>
          </div>
        </div>
      </div>
      <div className="py-28 md:px-5 bg-green-950 -mt-1 relative">
        <div className="container mx-auto">
          <Image
            className="hidden md:block md:absolute md:rotate-180 md:right-0 bottom-0 md:w-3/6"
            src={"/forms.svg"}
            alt={"forms"}
            height={500}
            width={500}
          />
          <p className="mb-28 text-center z-10">
            The Scouts of Macedonia is a national organization registered
            according to the Law on Associations and Foundations on November 19,
            1984, and registered as a youth organization in 2020 according to
            the Law on Youth Participation and Youth Policies. As an
            organization actively operating and existing since 1953, it
            implements life skills programs for children and youth throughout
            the entire country
          </p>
          <div className="grid grid-cols-3 gap-5">
            <div className="col-span-3 md:col-span-2 mb-5 md:mb-0 z-10">
              <div className="grid grid-cols-4 gap-5">
                <p className="hidden md:block md:col-span-1 md:text-3xl md:text-center md:-mt-10">
                  1921
                </p>
                <p className="md:hidden block col-span-4 text-3xl text-center">
                  1921- 1953
                </p>
                <p className="relative col-span-4 md:col-span-3 md:mb-14 p-5 md:p-10 rounded-3xl bg-zinc-400 text-black">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur possimus dolorem sequi? Omnis fuga saepe beatae
                  sunt reprehenderit pariatur iste sapiente? Ipsam nobis itaque
                  quidem quaerat, velit quis commodi illo tenetur omnis?
                  Quibusdam esse nemo saepe dolores, praesentium non, corrupti
                  unde deleniti dolor quod fuga a explicabo iusto odio! Alias
                  omnis eveniet magnam eos a laudantium architecto, quo labore
                  id, asperiores maxime fugiat beatae quod reprehenderit,
                  consequatur nesciunt. Natus cumque earum dignissimos cum
                  architecto tenetur? Nobis ab voluptatibus fugiat aliquam earum
                  ipsam quibusdam a, consectetur error accusantium enim
                  voluptas, dolor similique. Quidem voluptatibus dolorum at
                  architecto sunt blanditiis optio aperiam.
                  <svg
                    className="hidden md:block  absolute top-5 -bottom-5 h-full -left-[19%]"
                    width="6"
                    fill="none"
                    viewBox="0 0 6 422"
                  >
                    <path
                      fill="#fff"
                      d="M3 .333a2.667 2.667 0 1 0 0 5.334A2.667 2.667 0 0 0 3 .333Zm0 416a2.667 2.667 0 1 0 0 5.334 2.667 2.667 0 0 0 0-5.334ZM2.5 3v416h1V3h-1Z"
                    />
                  </svg>
                </p>
                <p className="hidden md:block md:col-span-1 text-3xl text-center -mt-10">
                  1953
                </p>
                <p className="md:hidden block col-span-4 text-3xl text-center">
                  1953- 1991
                </p>
                <p className="col-span-4 md:col-span-3 md:mb-14 p-5 md:p-10 rounded-3xl bg-zinc-400 text-black relative">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur possimus dolorem sequi? Omnis fuga saepe beatae
                  sunt reprehenderit pariatur iste sapiente? Ipsam nobis itaque
                  quidem quaerat, velit quis commodi illo tenetur omnis?
                  Quibusdam esse nemo saepe dolores, praesentium non, corrupti
                  unde deleniti dolor quod fuga a explicabo iusto odio! Alias
                  omnis eveniet magnam eos a laudantium architecto, quo labore
                  id, asperiores maxime fugiat beatae quod reprehenderit,
                  consequatur nesciunt. Natus cumque earum dignissimos cum
                  architecto tenetur? Nobis ab voluptatibus fugiat aliquam earum
                  ipsam quibusdam a, consectetur error accusantium enim
                  voluptas, dolor similique. Quidem voluptatibus dolorum at
                  architecto sunt blanditiis optio aperiam.
                  <svg
                    className="hidden md:block  absolute top-5 -bottom-5 h-full -left-[19%]"
                    width="6"
                    fill="none"
                    viewBox="0 0 6 422"
                  >
                    <path
                      fill="#fff"
                      d="M3 .333a2.667 2.667 0 1 0 0 5.334A2.667 2.667 0 0 0 3 .333Zm0 416a2.667 2.667 0 1 0 0 5.334 2.667 2.667 0 0 0 0-5.334ZM2.5 3v416h1V3h-1Z"
                    />
                  </svg>
                </p>
                <p className="hidden md:block md:col-span-1 text-3xl text-center -mt-10">
                  1991
                </p>
                <p className="md:hidden block col-span-4 text-3xl text-center">
                  1991 - 1997
                </p>
                <p className="col-span-4 md:col-span-3 md:mb-14 p-5 md:p-10 rounded-3xl bg-zinc-400 text-black relative">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur possimus dolorem sequi? Omnis fuga saepe beatae
                  sunt reprehenderit pariatur iste sapiente? Ipsam nobis itaque
                  quidem quaerat, velit quis commodi illo tenetur omnis?
                  Quibusdam esse nemo saepe dolores, praesentium non, corrupti
                  unde deleniti dolor quod fuga a explicabo iusto odio! Alias
                  omnis eveniet magnam eos a laudantium architecto, quo labore
                  id, asperiores maxime fugiat beatae quod reprehenderit,
                  consequatur nesciunt. Natus cumque earum dignissimos cum
                  architecto tenetur? Nobis ab voluptatibus fugiat aliquam earum
                  ipsam quibusdam a, consectetur error accusantium enim
                  voluptas, dolor similique. Quidem voluptatibus dolorum at
                  architecto sunt blanditiis optio aperiam.
                  <svg
                    className="hidden md:block  absolute top-5 -bottom-5 h-full -left-[19%]"
                    width="6"
                    fill="none"
                    viewBox="0 0 6 422"
                  >
                    <path
                      fill="#fff"
                      d="M3 .333a2.667 2.667 0 1 0 0 5.334A2.667 2.667 0 0 0 3 .333Zm0 416a2.667 2.667 0 1 0 0 5.334 2.667 2.667 0 0 0 0-5.334ZM2.5 3v416h1V3h-1Z"
                    />
                  </svg>
                </p>
                <p className="hidden md:block md:col-span-1 text-3xl text-center -mt-10">
                  1997
                </p>
                <p className="md:hidden block col-span-4 text-3xl text-center uppercase">
                  1997 - TODAY
                </p>
                <p className="col-span-4 md:col-span-3 md:mb-14 p-5 md:p-10 rounded-3xl bg-zinc-400 text-black relative">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur possimus dolorem sequi? Omnis fuga saepe beatae
                  sunt reprehenderit pariatur iste sapiente? Ipsam nobis itaque
                  quidem quaerat, velit quis commodi illo tenetur omnis?
                  Quibusdam esse nemo saepe dolores, praesentium non, corrupti
                  unde deleniti dolor quod fuga a explicabo iusto odio! Alias
                  omnis eveniet magnam eos a laudantium architecto, quo labore
                  id, asperiores maxime fugiat beatae quod reprehenderit,
                  consequatur nesciunt. Natus cumque earum dignissimos cum
                  architecto tenetur? Nobis ab voluptatibus fugiat aliquam earum
                  ipsam quibusdam a, consectetur error accusantium enim
                  voluptas, dolor similique. Quidem voluptatibus dolorum at
                  architecto sunt blanditiis optio
                  <svg
                    className="hidden md:block  absolute top-5 -bottom-5 h-full -left-[19%]"
                    width="6"
                    fill="none"
                    viewBox="0 0 6 422"
                  >
                    <path
                      fill="#fff"
                      d="M3 .333a2.667 2.667 0 1 0 0 5.334A2.667 2.667 0 0 0 3 .333Zm0 416a2.667 2.667 0 1 0 0 5.334 2.667 2.667 0 0 0 0-5.334ZM2.5 3v416h1V3h-1Z"
                    />
                  </svg>
                  aperiam.
                </p>
                <p className="hidden md:block md:col-span-1 text-3xl text-center -mt-10 uppercase">
                  Today
                </p>
                <p className="md:hidden block col-span-4 text-3xl text-center uppercase">
                  Now
                </p>
                <p className="col-span-4 md:col-span-3 p-5 md:p-10 rounded-3xl bg-zinc-400 text-black">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur possimus dolorem sequi? Omnis fuga saepe beatae
                  sunt reprehenderit pariatur iste sapiente? Ipsam nobis itaque
                  quidem quaerat, velit quis commodi illo tenetur omnis?
                  Quibusdam esse nemo saepe dolores, praesentium non, corrupti
                  unde deleniti dolor quod fuga a explicabo iusto odio! Alias
                  omnis eveniet magnam eos a laudantium architecto, quo labore
                  id, asperiores maxime fugiat beatae quod reprehenderit,
                  consequatur nesciunt. Natus cumque earum dignissimos cum
                  architecto tenetur? Nobis ab voluptatibus fugiat aliquam earum
                  ipsam quibusdam a, consectetur error accusantium enim
                  voluptas, dolor similique. Quidem voluptatibus dolorum at
                  architecto sunt blanditiis optio aperiam.
                </p>
              </div>
            </div>
            <p className="col-span-3 md:col-span-1 md:flex md:flex-wrap md:content-start z-10">
              <Image
                className="rounded-lg w-full mb-5 md:mb-14"
                src={"/1921.png"}
                alt={"1921"}
                height={500}
                width={500}
              />
              <Image
                className="rounded-lg w-full mb-5 md:mb-14"
                src={"/1997.png"}
                alt={"1997"}
                height={500}
                width={500}
              />
              <Image
                className="rounded-lg w-full"
                src={"/now.png"}
                alt={"now"}
                height={500}
                width={500}
              />
            </p>
          </div>
        </div>
      </div>
      <div className="py-28 md:px-5 text-black container mx-auto text-center">
        <h2 className="text-4xl uppercase mb-28">The scouting method</h2>
        <p className="mb-28 text-center z-10">
          The essential system for achieving the educational purpose of the
          scouting movement, defined as a system of progressive self-education.
          It represents a method based on the interaction of equally important
          elements that function together as a cohesive system, and the
          implementation of these elements in a combined and balanced manner is
          what makes scouting unique. The scouting method is the foundation of
          scouting and is expressed through the following elements:
        </p>
        <Image
          className="w-full lg:w-3/6 mx-auto"
          src={"/The scouting method.png"}
          alt={"forms"}
          height={1000}
          width={1000}
        />
      </div>
      <div className="py-28 md:px-5 bg-gradient-to-b via-green-950 from-green-950 to-transparent relative">
        <Image
          className="absolute -top-[52px] left-[50%] -translate-x-2/4 rotate-180"
          src={"/icons/triangle.svg"}
          alt={"triangle figures"}
          height={100}
          width={100}
        />
        <div className="container mx-auto text-center">
          <Image
            className="absolute rotate-90 right-2 top-0"
            src={"/forms.svg"}
            alt={"forms"}
            height={250}
            width={250}
          />

          <h2 className="mb-10 text-4xl uppercase relative z-10">Our vision</h2>
          <p className="mb-10 relative z-10">
            The Scouts of Macedonia is a national organization registered
            according to the Law on Associations and Foundations on November 19,
            1984, and registered as a youth organization in 2020 according to
            the Law on Youth Participation and Youth Policies. As an
            organization actively operating and existing since 1953, it
            implements life skills programs for children and youth throughout
            the entire country
          </p>
          <p className="uppercase relative z-20 text-2xl">Be ready</p>
        </div>
      </div>
    </>
  );
}
