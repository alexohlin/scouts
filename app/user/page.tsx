"use client";
import Loading from "@/components/Loading";
import { useUser } from "@clerk/nextjs";
import { ClerkLoaded } from "@clerk/nextjs";
import { redirect } from "next/navigation";

export default function RedirectUserPage() {
  const { user } = useUser();

  <Loading />;

  <ClerkLoaded>{redirect(`user/${user?.username}`)};</ClerkLoaded>;
}
