"use client";
import Loading from "@/components/Loading";
import { UserProfile, useUser } from "@clerk/nextjs";
import { redirect } from "next/navigation";

export default function Page({ params }: { params: { slug: string } }) {
  const { user } = useUser();

  if (user) {
    if (params.slug !== user.username?.toString()) {
      redirect(`/user/${user?.username}`);
    }
  }

  return (
    <>
      <Loading />
      <UserProfile
        appearance={{
          variables: { colorPrimary: "#213C33", colorWarning: "#EF767A" },
          elements: { rootBox: "container py-5", card: "mx-auto" },
        }}
      />
    </>
  );
}
