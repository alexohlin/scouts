import Loading from "@/components/Loading";
import { SignUp } from "@clerk/nextjs";

export default function SignUpPage() {
  return (
    <>
      <Loading />
      <SignUp
        afterSignInUrl={"/"}
        appearance={{
          variables: { colorPrimary: "#213C33", colorWarning: "#EF767A" },
          elements: { rootBox: "container mx-auto py-5" },
        }}
      />
    </>
  );
}
