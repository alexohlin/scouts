import { SignIn } from "@clerk/nextjs";

export default function SignInPage() {
  return (
    <SignIn
      afterSignInUrl={"/"}
      appearance={{
        variables: { colorPrimary: "#213C33", colorWarning: "#EF767A" },
        elements: { rootBox: "container py-5", card: "mx-auto" },
      }}
    />
  );
}
