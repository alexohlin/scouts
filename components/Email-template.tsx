export default function EmailTemplate({
  firstname,
  lastname,
  message,
  email,
}: {
  firstname: string;
  email: string;
  lastname: string;
  message: string;
}) {
  return (
    <div className="p-5">
      <p className="text-2xl">
        Message from {firstname} {lastname}
      </p>
      <p className="mb-5">{email}</p>
      <p>{message}</p>
    </div>
  );
}
