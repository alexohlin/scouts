import { useFormStatus } from "react-dom";

export default function SubmitButton() {
  const { pending } = useFormStatus();

  return (
    <button
      disabled={pending}
      aria-disabled={pending}
      className={`bg-orange-400 hover:bg-orange-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline ${
        pending ? "disabled:bg-zinc-500" : ""
      }`}
      type="submit"
    >
      {pending ? "Submitting..." : "Submit"}
    </button>
  );
}
