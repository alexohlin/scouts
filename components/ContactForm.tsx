"use client";
import { ContactFormAction } from "@/app/actions/ContactFormAction";
import { useEffect, useRef, useState } from "react";
import SubmitButton from "./SubmitButton";

interface ContactForm {
  email: string;
  firstname: string;
  lastname: string;
  message: string;
}

export default function ContactForm() {
  const regex = /^\S+@\S+\.\S+$/;
  const ref = useRef<HTMLFormElement>(null);
  const [isSent, setIsSent] = useState(false);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [
    clickedSubmitWhileInputsAreInvalid,
    setClickedSubmitWhileInputsAreInvalid,
  ] = useState(false);
  const [data, setData] = useState<ContactForm>({
    email: "",
    firstname: "",
    lastname: "",
    message: "",
  });
  const [isFormComplete, setIsFormComplete] = useState(false);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  useEffect(() => {
    setIsValidEmail(regex.test(data.email));

    const isComplete =
      isValidEmail &&
      data.email !== "" &&
      data.firstname !== "" &&
      data.lastname !== "" &&
      data.message !== "";

    setIsFormComplete(isComplete);
  }, [data, isValidEmail]);

  return (
    <form
      ref={ref}
      action={async (FormData) => {
        try {
          const response: any = await ContactFormAction(FormData);
          if (response && "error" in response) {
            alert(response.error.message);
          } else {
            ref.current?.reset();
            setIsSent(true);
          }
        } catch (error) {
          console.error("An error occurred:", error);
        }
      }}
      className="bg-green-950 rounded-3xl w-full px-10 py-5 relative"
    >
      {isSent && (
        <div className="p-10 py-28 absolute top-0 bottom-0 left-0 right-0 bg-[#052e16f2] rounded-3xl">
          <p className="text-white text-center text-3xl mb-5">
            Thank you for your message
          </p>
          <button
            className="bg-orange-400 p-5 text-center text-white rounded-3xl absolute left-[50%] -translate-x-2/4"
            onClick={() => setIsSent(false)}
          >
            Send Antoher one
          </button>
        </div>
      )}
      <div className="mb-4">
        <label
          className="block uppercase text-gray-300 mb-2"
          htmlFor="firstname"
        >
          firstname <span className="lowercase">(required)</span>
        </label>
        <input
          required
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-300 leading-tight focus:outline-none focus:shadow-outline"
          id="firstname"
          name="firstname"
          type="text"
          placeholder="John"
          onChange={handleInputChange}
        />
        {clickedSubmitWhileInputsAreInvalid && data.firstname === "" && (
          <small className="text-red-200">First name not valid</small>
        )}
      </div>
      <div className="mb-4">
        <label
          className="block uppercase text-gray-300 mb-2"
          htmlFor="lastname"
        >
          lastname <span className="lowercase">(required)</span>
        </label>
        <input
          required
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-300 leading-tight focus:outline-none focus:shadow-outline"
          id="lastname"
          name="lastname"
          type="text"
          placeholder="Doe"
          onChange={handleInputChange}
        />
        {clickedSubmitWhileInputsAreInvalid && data.lastname === "" && (
          <small className="text-red-200">Last name not valid</small>
        )}
      </div>
      <div className="mb-4">
        <label className="block text-gray-300 mb-2 uppercase" htmlFor="email">
          Email <span className="lowercase">(required)</span>
        </label>
        <input
          required
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-300 leading-tight focus:outline-none focus:shadow-outline"
          id="email"
          type="email"
          name="email"
          placeholder="john@doe.com"
          onChange={handleInputChange}
        />
        {clickedSubmitWhileInputsAreInvalid &&
          (data.email === "" || !isValidEmail) && (
            <small className="text-red-200">Email not valid</small>
          )}
      </div>{" "}
      <div className="mb-4">
        <label className="block uppercase text-gray-300 mb-2" htmlFor="message">
          Messsage <span className="lowercase">(required)</span>
        </label>
        <textarea
          className="shadow appearance-none border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
          required
          id="message"
          name="message"
          onChange={handleInputChange}
        />
        {clickedSubmitWhileInputsAreInvalid && data.message === "" && (
          <small className="text-red-200">Please write your message</small>
        )}
      </div>
      {isFormComplete ? (
        <SubmitButton />
      ) : (
        <button
          onClick={(e) => {
            e.preventDefault();
            setClickedSubmitWhileInputsAreInvalid(true);
          }}
          className={
            "text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline bg-zinc-500"
          }
          type="button"
        >
          Submit
        </button>
      )}
    </form>
  );
}
