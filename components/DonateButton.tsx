"use client";

import axios from "axios";

export default function DonateButton({styleClasses}: {styleClasses:string}) {
  const donate = async () => {
    try {
      const response = await axios.post("/api/donation", {
        productId: "291677",
      });

      console.log(response.data);
      window.open(response.data.checkoutUrl, "_blank");
    } catch (error) {
      console.log(error);
      alert("failed to donate");
    }
  };

  return (
    <button onClick={donate} className={styleClasses}>
      Donate
    </button>
  );
}
