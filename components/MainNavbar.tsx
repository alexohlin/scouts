"use client";
import Link from "next/link";
import Image from "next/image";
import React, { useState } from "react";
import { usePathname } from "next/navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons/faArrowDown";
import { faArrowUp } from "@fortawesome/free-solid-svg-icons/faArrowUp";
import { UserButton, useUser } from "@clerk/nextjs";

export function MainNavbar() {
  const [open, setOpen] = useState(false);
  const [projectsSubmenuOpen, setProjectsSubmenuOpen] = useState(false);
  const path = usePathname();
  const { user, isSignedIn } = useUser();
  const isSignInOrSignUp =
    path.includes("/sign-in") || path.includes("/sign-up");

  return (
    <div
      className={`flex items-center justify-between p-5 relative z-10 ${
        isSignInOrSignUp || path.includes("/user") ? "text-zinc-800" : ""
      }`}
    >
      <Link href="/" className="block">
        <Image
          src={
            isSignInOrSignUp || path.includes("/user")
              ? "/logo-green.svg"
              : "/logo.svg"
          }
          alt="logo"
          width={50}
          height={50}
        />
      </Link>
      <div className="flex w-full items-center justify-end relative md:text-2xl">
        <button
          onClick={() => setOpen(!open)}
          id="navbarToggler"
          className={`right-4 absolute lg:hidden ${
            open ? "navbarTogglerActive" : "block"
          } `}
        >
          <span
            className={`relative my-[6px] block h-[2px] w-[30px] bg-body-color ${
              path.includes("/user") ? "bg-green-950" : "dark:bg-white"
            }`}
          />
          <span
            className={`relative my-[6px] block h-[2px] w-[30px] bg-body-color ${
              path.includes("/user") ? "bg-green-950" : "dark:bg-white"
            }`}
          />
          <span
            className={`relative my-[6px] block h-[2px] w-[30px] bg-body-color ${
              path.includes("/user") ? "bg-green-950" : "dark:bg-white"
            }`}
          />
        </button>
        <nav
          id="navbarCollapse"
          className={`absolute top-4 right-0 bg-green-950 bg-opacity-80 lg:bg-transparent p-5 lg:p-0 lg:static ${
            !open && "hidden lg:flex"
          } ${path.includes("/user") ? "text-white md:text-black" : ""} `}
        >
          <Link
            onClick={() => setOpen(false)}
            href="/"
            className={`${
              path === "/" ? "text-green-400 font-bold" : "hover:text-green-400"
            } flex mb-5 lg:mb-0 lg:mr-12 lg:inline-flex`}
          >
            Home
          </Link>
          <Link
            onClick={() => setOpen(false)}
            href="/forus"
            className={`${
              path === "/forus"
                ? "text-green-400 font-bold"
                : "hover:text-green-400"
            } flex mb-5 lg:mb-0 lg:mr-12 lg:inline-flex`}
          >
            For Us
          </Link>
          <div
            className="relative lg:inline-flex"
            onMouseEnter={() => setProjectsSubmenuOpen(true)}
            onMouseLeave={() => setProjectsSubmenuOpen(false)}
          >
            <Link
              onClick={() => setOpen(false)}
              href="/programs"
              className={`${
                path.includes("/programs")
                  ? "text-green-400 font-bold"
                  : "hover:text-green-400"
              } flex mb-5 lg:mb-0 lg:mr-12 lg:inline-flex`}
            >
              Programs
              <FontAwesomeIcon
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setProjectsSubmenuOpen(!projectsSubmenuOpen);
                }}
                className="lg:hidden ml-2 my-auto"
                icon={projectsSubmenuOpen ? faArrowUp : faArrowDown}
              />
            </Link>
            <div
              className={`${
                !projectsSubmenuOpen
                  ? "hidden"
                  : `lg:absolute lg:top-8 lg:-bottom-32s lg:left-0 bg-green-950 ${
                      isSignInOrSignUp || path.includes("/user")
                        ? "bg-opacity-30"
                        : "bg-opacity-90"
                    }  p-2 lg:p-5 mb-5 lg:mb-0`
              }`}
            >
              <Link
                className={`${
                  path.includes("/juniors")
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex mb-5`}
                onClick={() => {
                  setProjectsSubmenuOpen(false);
                  setOpen(false);
                }}
                href="/programs/juniors"
              >
                Juniors
              </Link>
              <Link
                className={`${
                  path.includes("/scouts")
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex mb-5`}
                onClick={() => {
                  setProjectsSubmenuOpen(false);
                  setOpen(false);
                }}
                href="/programs/scouts"
              >
                Scouts
              </Link>
              <Link
                className={`${
                  path.includes("/researchers")
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex mb-5`}
                onClick={() => {
                  setProjectsSubmenuOpen(false);
                  setOpen(false);
                }}
                href="/programs/researchers"
              >
                Researchers
              </Link>
              <Link
                className={`${
                  path.includes("/rovers")
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex mb-5`}
                onClick={() => {
                  setProjectsSubmenuOpen(false);
                  setOpen(false);
                }}
                href="/programs/rovers"
              >
                Rovers
              </Link>
              <Link
                className={`${
                  path.includes("/seniors")
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex`}
                onClick={() => {
                  setProjectsSubmenuOpen(false);
                  setOpen(false);
                }}
                href="/programs/seniors"
              >
                Seniors
              </Link>
            </div>
          </div>
          <Link
            onClick={() => setOpen(false)}
            href="/projects"
            className={`${
              path === "/projects"
                ? "text-green-400 font-bold"
                : "hover:text-green-400"
            } flex mb-5 lg:mb-0 lg:mr-12 lg:inline-flex`}
          >
            Projects
          </Link>
          <Link
            onClick={() => setOpen(false)}
            href="/blog"
            className={`${
              path === "/blog"
                ? "text-green-400 font-bold"
                : "hover:text-green-400"
            } flex mb-5 lg:mb-0 lg:mr-12 lg:inline-flex`}
          >
            Blog
          </Link>
          {!isSignInOrSignUp && (
            <div className="inline-flex">
              <Link
                onClick={() => setOpen(false)}
                href={isSignedIn ? `/user/${user.username}` : "/user"}
                className={`${
                  path === "/user"
                    ? "text-green-400 font-bold"
                    : "hover:text-green-400"
                } flex lg:inline-flex ${user ? "mr-5 items-center" : ""}`}
              >
                {user ? user.firstName : "Sign In"}
              </Link>
              <UserButton afterSignOutUrl="/" />
            </div>
          )}
        </nav>
      </div>
    </div>
  );
}
