"use client";
import Link from "next/link";
import Image from "next/image";
import { usePathname } from "next/navigation";

const Footer = () => {
  const path = usePathname();
  const isSignInOrSignUp =
    path.includes("/sign-in") || path.includes("/sign-up");

  return (
    !isSignInOrSignUp && (
      <footer
        className="bg-no-repeat"
        style={{
          backgroundImage: `url('/footer.svg')`,
        }}
      >
        <div className="p-10 lg:p-28 container mx-auto">
          <Link href="/" className="block mb-10">
            <Image
              src="logo.svg"
              alt="logo"
              width={50}
              height={50}
              className="mx-auto"
            />
          </Link>
          <div className="flex flex-wrap mb-10 justify-between">
            <div className="w-1/2 md:w-full md-flex-wrap flex flex-col md:flex-row justify-between md:mb-10">
              <Link
                href="/contact"
                className={`${
                  path === "/contact"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Contact
              </Link>
              <Link
                href="/careers"
                className={`${
                  path === "/careers"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Careers
              </Link>
              <Link
                href="/privacy"
                className={`${
                  path === "/privacy"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Privacy
              </Link>
              <Link
                href="/projects"
                className={`${
                  path === "/projects"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Projects
              </Link>
              <Link
                href="/help"
                className={`${
                  path === "/help"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Help
              </Link>
              <Link
                href="/cookiespolicy"
                className={`${
                  path === "/cookiespolicy"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                } mr-5 lg:mr-10`}
              >
                Cookies policy
              </Link>
              <Link
                href="/newsletter"
                className={`${
                  path === "/newsletter"
                    ? "text-green-600 font-bold"
                    : "hover:text-green-600"
                }`}
              >
                Newsletter
              </Link>
            </div>
            <div className="w-1/2 md:w-full md-flex-wrap flex flex-col md:flex-row justify-between items-end md:justify-center">
              <div className="mb-5 md:mb-0 md:mr-10 md:inline-flex">
                <a
                  href="/#"
                  className="
                hover:text-green-600 mb-2 md:mb-0 block md:inline-flex"
                >
                  <Image
                    src="/tiktok.png"
                    alt="tiktok icon"
                    width={30}
                    height={30}
                    style={{ height: "30px" }}
                    className="object-contain"
                  />
                </a>
                <a
                  href="/#"
                  className="
                hover:text-green-600 mb-2 md:mb-0 block md:inline-flex"
                >
                  <Image
                    src="/facebook.png"
                    alt="facebook icon"
                    width={30}
                    height={30}
                    style={{ height: "30px" }}
                    className="object-contain"
                  />
                </a>
                <a
                  href="#"
                  className="
            hover:text-green-600 md:inline-flex"
                >
                  <Image
                    src="/instagram.png"
                    alt="instagram icon"
                    width={30}
                    height={30}
                    style={{ height: "30px" }}
                    className="object-contain"
                  />
                </a>
              </div>
              <a
                href="#"
                className="hover:text-green-600 inline-flex md:mr-10 items-center"
              >
                <Image
                  src="/mail.png"
                  alt="mail icon"
                  width={30}
                  height={30}
                  style={{ height: "30px" }}
                  className="object-contain mr-1 md:mr-5"
                />
                sim@scout.org.mk
              </a>
              <a href="#" className="hover:text-green-600 inline-flex">
                <Image
                  src="/phone.png"
                  alt="phone icon"
                  width={30}
                  height={30}
                  style={{ height: "30px" }}
                  className="object-contain mr-1 md:mr-5"
                />
                02 311 2254
              </a>
            </div>
          </div>
          <p className="text-center">© 2023 Сојуз на Извидници на Македонија</p>
        </div>
      </footer>
    )
  );
};

export default Footer;
